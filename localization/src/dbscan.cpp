#include "localization/dbscan.h"
#include <ros/ros.h>

const int mn_points = 0;

int DBSCAN::run()
{
    int clusterID = 1;
    vector<Point>::iterator iter;
    //ROS_INFO("DS RUN BEGIN");
    for(iter = m_points.begin(); iter != m_points.end(); ++iter)
    {
        if ( iter->clusterID == UNCLASSIFIED )
        {
            //ROS_INFO ("EXPAND BEGIN");
            if ( expandCluster(*iter, clusterID) != FAILURE )
            {
                clusterID += 1;
            }
            //ROS_INFO("EXPAND END");

        }
    }
    //ROS_INFO("DS RUN END");
    return 0;
}

int DBSCAN::expandCluster(Point&  point, int clusterID)
{
    vector<int> clusterSeeds = calculateCluster(point);

    int csize= clusterSeeds.size();
    if ( csize > mn_points && csize < m_minPoints )
    {
        //ROS_INFO("CLUSTER SIZE IS %d",csize);
        point.clusterID = NOISE;
        //return FAILURE;
        return true;
    }
    else
    {
        int index = 0, indexCorePoint = 0;
        vector<int>::iterator iterSeeds;
        for( iterSeeds = clusterSeeds.begin(); iterSeeds != clusterSeeds.end(); ++iterSeeds)
        {
            m_points.at(*iterSeeds).clusterID = clusterID;
            if (m_points.at(*iterSeeds).x == point.x && m_points.at(*iterSeeds).y == point.y && m_points.at(*iterSeeds).z == point.z )
            {
                indexCorePoint = index;
            }
            ++index;
        }
        clusterSeeds.erase(clusterSeeds.begin()+indexCorePoint);

        for( vector<int>::size_type i = 0, n = clusterSeeds.size(); i < n; ++i )
        {
            vector<int> clusterNeighors = calculateCluster(m_points.at(clusterSeeds[i]));

            if ( clusterNeighors.size() >= m_minPoints )
            {
                vector<int>::iterator iterNeighors;
                for ( iterNeighors = clusterNeighors.begin(); iterNeighors != clusterNeighors.end(); ++iterNeighors )
                {
                    //if ( m_points.at(*iterNeighors).clusterID == UNCLASSIFIED || m_points.at(*iterNeighors).clusterID == NOISE )
                    //{
                        if ( m_points.at(*iterNeighors).clusterID == UNCLASSIFIED )
                        {
                            clusterSeeds.push_back(*iterNeighors);
                            n = clusterSeeds.size();
                        }
                        m_points.at(*iterNeighors).clusterID = clusterID;
                    //}
                }
            }
        }

        return SUCCESS;
    }
}

vector<int> DBSCAN::calculateCluster(Point point)
{
    int index = 0;
    vector<Point>::iterator iter;
    vector<int> clusterIndex;
    for( iter = m_points.begin(); iter != m_points.end(); ++iter)
    {
        double dis=calculateDistance(point, *iter);
        //ROS_INFO("DISTANCE IS %f",dis);
        if ( dis <= m_epsilon )
        {
            //ROS_INFO("IN DIS CHECK");
            //ROS_INFO("m_epsilon IS %f",m_epsilon);
            clusterIndex.push_back(index);
        }
        index++;
    }
    return clusterIndex;
}

inline double DBSCAN::calculateDistance( Point pointCore, Point pointTarget )
{
    return pow(pointCore.x - pointTarget.x,2)+pow(pointCore.y - pointTarget.y,2);
}
