//"Leistungskurs C++ Turtlebot"
//"color Detecktion"
//"Grupp2"
//"Subscriber_node"



#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

//#include "std_msgs/String.h"
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/Point.h>
#include <vector>

using namespace std;
using namespace cv;
using namespace cv_bridge;

Mat TeamImage;

/*
Mat DepthImage;


void imageCallback(const sensor_msgs::ImageConstPtr &depth) {
  cv_bridge::CvImagePtr cv_ptr;
  cv_ptr = cv_bridge::toCvCopy(depth, sensor_msgs::image_encodings::TYPE_32FC1);
  DepthImage = cv_ptr->image;

  res.distance_ = DepthImage.at<float>(200, 300);

  namedWindow("DepthImage image", WINDOW_AUTOSIZE);
  imshow("DepthImage image", DepthImage);

  waitKey(1);
}
*/


// Morph_filter anti noise
// use erode and dilate to eliminate the noise and remain object(binary image)
void Morph_filter(Mat &mask)
{

  Mat erodeKernal = getStructuringElement(MORPH_RECT, Size(5,5));
  Mat dilateKernal = getStructuringElement(MORPH_RECT, Size(5,5));

  erode(mask, mask, erodeKernal);
  dilate(mask, mask, dilateKernal);


}

void teamdetection(Mat Yellowmask, Mat Bluemask) {

Mat tempyellow;
Mat tempblue;

Yellowmask.copyTo(tempyellow);;
Bluemask.copyTo(tempblue);

vector< vector <Point> > contours_yellow;
vector<Vec4i> hierarchy_yellow;

vector< vector <Point> > contours_blue;
vector<Vec4i> hierarchy_blue;

findContours(tempyellow, contours_yellow, hierarchy_yellow, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
findContours(tempblue, contours_blue, hierarchy_blue, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);


bool teamyellow = false;
bool teamblue = false;

double area_yellow = 0;
double area_blue = 0;

for (int index = 0; index >= 0; index = hierarchy_yellow[index][0]){
  Moments moment_yellow = moments((cv::Mat)contours_yellow[index]);
  area_yellow = moment_yellow.m00;
  ROS_INFO_STREAM("area_yellow"<<area_yellow);
}

for (int index = 0; index >= 0; index = hierarchy_blue[index][0]){
  Moments moment_blue = moments((cv::Mat)contours_blue[index]);
  area_blue = moment_blue.m00;
  ROS_INFO_STREAM("area_blue"<<area_blue);
}

if (area_yellow = area_blue) {
  //ROS_INFO("Not detection prosition");
}
else if(area_yellow > area_blue)
{
  teamyellow = true;
}
else{
  teamblue = true;
}

}


void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
  cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    TeamImage = cv_ptr->image;
    //namedWindow("original image", WINDOW_AUTOSIZE);
    //imshow("original image", TeamImage);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());

}

Mat HSV;
Mat HSV1;
Mat Bluemask;
Mat Yellowmask;

cvtColor(TeamImage, HSV, COLOR_BGR2HSV);
cvtColor(TeamImage, HSV1, COLOR_BGR2HSV);

inRange(HSV, Scalar(18, 210, 43), Scalar(31, 256, 256), Yellowmask); // use rosbag track yellow
inRange(HSV1, Scalar(100, 50, 46), Scalar(130, 255, 255), Bluemask); // use rosbag track blue

//Morph_filter(Yellowmask);

namedWindow("Morph_filter_yellow", WINDOW_AUTOSIZE);
imshow("Morph_filter_yellow",Yellowmask);

//Morph_filter(Bluemask);
namedWindow("Morph_filter_blue", WINDOW_AUTOSIZE);
imshow("Morph_filter_blue",Bluemask);

teamdetection(Yellowmask,Bluemask);

waitKey(1);

}



int main(int argc, char **argv)
{
  ros::init(argc,argv, "TeamColorDetection");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  image_transport::Subscriber sub = it.subscribe("/camera/rgb/image_raw",1,imageCallback);


  //pub = nh.advertise<geometry_msgs::Point>("/teamcolor", 1);


  ros::Rate loop_rate(10);


  ros::spin();
  return 0;
}
