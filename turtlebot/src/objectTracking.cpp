//"Leistungskurs C++ Turtlebot"
//"Object Tracking"
//"Grupp2"
//"Subscriber_node"



#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CompressedImage.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Point.h>
#include <turtlebot/gotcolor.h>

//#include "std_msgs/String.h"
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/CompressedImage.h>
#include <vector>
using namespace std;
using namespace cv;
using namespace cv_bridge;

//ros
ros::Publisher pub_pose;

//Image
Mat Image;

// HSV
int H_min = 0;
int H_max = 255;
int S_min = 0;
int S_max = 255;
int V_min = 0;
int V_max = 255;

// findContours
int frame_width = 640;
int frame_height = 480;

int Max_object_num = 30;
int Min_object_area = 10*10; //20*20;
int Max_object_area = frame_width*frame_height*3/2;

// draw object
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;
string color = "green";



// Trackbars
void trackbar_nothing(int, void*){
}

void createTrackbars() {
  namedWindow("Trackbars",WINDOW_AUTOSIZE);
  createTrackbar("H_min","Trackbars", &H_min, H_max, trackbar_nothing);
  createTrackbar("H_max","Trackbars", &H_min, H_max, trackbar_nothing);
  createTrackbar("S_min","Trackbars", &S_min, S_max, trackbar_nothing);
  createTrackbar("S_max","Trackbars", &S_min, S_max, trackbar_nothing);
  createTrackbar("V_min","Trackbars", &V_min, V_max, trackbar_nothing);
  createTrackbar("V_max","Trackbars", &V_min, V_max, trackbar_nothing);

}


// Morph_filter anti noise
// use erode and dilate to eliminate the noise and remain object(binary image)
void Morph_filter(Mat &mask)
{

  Mat erodeKernal = getStructuringElement(MORPH_RECT, Size(5,5));
  Mat dilateKernal = getStructuringElement(MORPH_RECT, Size(5,5));

  erode(mask, mask, erodeKernal);
  dilate(mask, mask, dilateKernal);


}



string intToString(int number)
{
        std::stringstream ss;
        ss << number;
        return ss.str();
}


// draw object position
void drawposition(int x, int y, Mat & Image){
  circle(Image, Point(x, y), 40, Scalar(0, 255, 0), 2); //50
        if (y - 25 > 0)
                line(Image, Point(x, y), Point(x, y - 25), Scalar(0, 255, 0), 2);
        else
                line(Image, Point(x, y), Point(x, 0), Scalar(0, 255, 0), 2);
        if (y + 25 < FRAME_HEIGHT)
                line(Image, Point(x, y), Point(x, y + 25), Scalar(0, 255, 0), 2);
        else
                line(Image, Point(x, y), Point(x, FRAME_HEIGHT), Scalar(0, 255, 0), 2);
        if (x - 25 > 0)
                line(Image, Point(x, y), Point(x - 25, y), Scalar(0, 255, 0), 2);
        else
                line(Image, Point(x, y), Point(0, y), Scalar(0, 255, 0), 2);
        if (x + 25 < FRAME_WIDTH)
                line(Image, Point(x, y), Point(x + 25, y), Scalar(0, 255, 0), 2);
        else
                line(Image, Point(x, y), Point(FRAME_WIDTH, y), Scalar(0, 255, 0), 2);

        putText(Image, intToString(x) + "," + intToString(y), Point(x, y + 30), 1, 1, Scalar(0, 255, 0), 2);
}





// Using findContours of OpenCV to find biggest area in HSV image
void track_object(int &x, int &y, Mat mask, Mat Image) {
  Mat temp;
  mask.copyTo(temp);

  // definite parameters for findContours
  vector< vector <Point> > contours;
  vector<Vec4i> hierarchy;
  // find contours of image
  findContours(temp, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE); //RETR_CCOMP, build 2 hierarchies; CHAIN_APPROX_SIMPLE, only save the final points

//  ROS_INFO_STREAM("" << contours);

  bool objectFound = false;  // find object or not
  double ref_area1 = 0;   // use as a reference area for finding the biggest area in the picture
  double ref_area2 = 0;   // use as a reference area for finding the biggest area in the picture
  double ref_area3 = 0;   // use as a reference area for finding the biggest area in the picture
  int x1 = 0;
  int x2 = 0;
  int x3 = 0;
  int y1 = 0;
  int y2 = 0;
  int y3 = 0;
  float length = 0;
  float length1 = 0;
  float length2 = 0;
  float length3 = 0;


  if (hierarchy.size() > 0)
  {  // find object or not
    int find_num = contours.size(); //  how many contours do I find
    // ROS_INFO("find_num %d",find_num);
    int contours_info = contours.size();
  //  ROS_INFO("contours_info %d",contours_info);
  //  int index = hierarchy[0][0];
  //  ROS_INFO("hierarchy index %d", index);



    if (find_num < Max_object_num )
    {
        for (int index = 0; index >= 0; index = hierarchy[index][0]) // choose which contoue(object) we should to track
      //  ROS_INFO("index %d",index);
        {

            Moments moment = moments((cv::Mat)contours[index]);
            double area = moment.m00;
            length = arcLength((cv::Mat)contours[index], true);
          // track the biggest area, depending on the ref_area and area
          if (area > Min_object_area && area < Max_object_area  )  // if the detected object satisfy the defined range
          {
              x = moment.m10 / area;
              y = moment.m01 / area;
              objectFound = true;

              if( x1 == 0)
              {
                  x1 = x;
                  y1 = y;
                  ref_area1 = area;
                  length1 = length;
              }
              else if (x2 == 0)
              {
                  x2 = x;
                  y2 = y;
                  ref_area2 = area;
                  length2 = length;
              }
              else if (x3 == 0) {
                  x3 = x;
                  y3 = y;
                  ref_area3 = area;
                  length3 = length;

              }
              else if ( ref_area1 < area )
              {
                 if(ref_area1 > ref_area2)
                 {
                     if( ref_area2 > ref_area3)
                     {
                         ref_area3 = ref_area2;
                         x3 = x2;
                         y3 = y2;
                         length3 = length2;
                     }
                     ref_area2 = ref_area1;
                     x2 = x1;
                     y2 = y1;
                     length2 = length1;
                 }
                 else if ( ref_area1 > ref_area3 )
                 {
                     ref_area3 = ref_area1;
                     x3 = x1;
                     y3 = y1;
                     length3 = length1;
                 }
                 ref_area1 = area;
                 x1 = x;
                 y1 = y;
                 length1 = length;
              }
              else if (ref_area2 < area)
              {
                  if( ref_area2 > ref_area3)
                  {
                      ref_area3 = ref_area2;
                      x3 = x2;
                      y3 = y2;
                      length3 = length2;
                  }
                  ref_area2 = area;
                  x2 = x;
                  y2 = y;
                  length2 = length;
              }
              else if( ref_area3 < area )
              {
                  ref_area3 = area;
                  x3 = x;
                  y3 = y;
                  length3 = length;
              }

              //ROS_INFO("x_info %d",x);
              //ROS_INFO("y_info %d",y);
          }
          else
          {
              objectFound = false;
          }
        }
        //ROS_INFO_STREAM("time: " << time);

      if (objectFound == true)
      {
        //drawposition(x1, y1, Image); // draw goal in original image
        //drawposition(x2, y2, Image); // draw goal in original image
        //drawposition(x3, y3, Image); // draw goal in original image
        geometry_msgs::Point msg;
        x1 = max( x1, x2);
        x1 = max( x1, x3);
        drawposition(x1, y1, Image); // draw goal in original image
        msg.x = x1;
        msg.y = y1+242;
        pub_pose.publish(msg);
      }
    }

  }
  else
  {
    geometry_msgs::Point msg;
    msg.x = 0;
    msg.y = 0;
    //ROS_INFO_STREAM(__LINE__);
    pub_pose.publish(msg);
  }
}


/*
// callback original image from camera and combine HSV method
void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{

    cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    Image = cv_ptr->image;
    //namedWindow("original image", WINDOW_AUTOSIZE);
    //imshow("original image", Image);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());

}


// HSV method
int x = 0;
int y = 0;

Mat HSV;
Mat mask;

createTrackbars();

cvtColor(Image, HSV, COLOR_BGR2HSV);

//inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);
if( color == "yellow" )
{
    //inRange(HSV, Scalar(18, 210, 43), Scalar(31, 256, 256), mask); // use rosbag track yellow
    inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);

}
else if ( color == "blue" )
{
    // the color blue is current not perfect, coz the line on the floor and much noise
    //inRange(HSV, Scalar(125, 80, 46), Scalar(125, 255, 255), mask); // use rosbag track blue
    inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);

}
else if ( color == "blue" )
{
    // the color blue is current not perfect, coz the line on the floor and much noise
    //inRange(HSV, Scalar(125, 80, 46), Scalar(125, 255, 255), mask); // use rosbag track blue
    inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);

}


// for yellow (26，35，44 / 255, 44, 255)
//namedWindow("HSV", WINDOW_AUTOSIZE);
//imshow("HSV",mask);

// Do image filting
Morph_filter(mask);
namedWindow("Morph_filter", WINDOW_AUTOSIZE);
imshow("Morph_filter",mask);


// before track should do filtering
track_object(x, y, mask, Image);
// drwa the image with x,y position, which is the object prosition in image
//namedWindow("Object Tracking", WINDOW_AUTOSIZE);
//imshow("Object Tracking",Image);



  waitKey(1);
}*/

void compressed_callback(const sensor_msgs::CompressedImageConstPtr& msg)
{
    try {
        Image = cv::imdecode(cv::Mat(msg->data),1);
        Rect rect(50, 242, 520, 100);
        Image = Image(rect);
        //ROS_INFO_STREAM("size: " << Image.size);
        //namedWindow("original image", WINDOW_AUTOSIZE);
        //imshow("original image", Image);
    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("Could not convert to image");

    }
    // HSV method
    int x = 0;
    int y = 0;

    Mat HSV;
    Mat mask;

    //createTrackbars();

    cvtColor(Image, HSV, COLOR_BGR2HSV);

    if( color == "yellow" )
    {
        //inRange(HSV, Scalar(18, 210, 43), Scalar(31, 256, 256), mask); // use rosbag track yellow
        inRange(HSV, Scalar(18, 110, 150), Scalar(31, 256, 256), mask); // use rosbag track yellow
        // min_s important
        //inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);
    }
    else if( color == "blue" )
    {
        // the color blue is current not perfect, coz the line on the floor and much noise
        //inRange(HSV, Scalar(100, 50, 120), Scalar(150, 255, 200), mask); // for collor detection


        //inRange(HSV, Scalar(100, 50, 75), Scalar(150, 255, 150), mask); // use rosbag track blue

        inRange(HSV, Scalar(100, 100, 40), Scalar(150, 255, 255), mask); // for blue ball
        // min_V important

        //inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);

    }
    else
    {
        // the color green is current not perfect, coz the line on the floor and much noise

        inRange(HSV, Scalar(40, 40, 40), Scalar(70, 255, 255), mask); // for blue ball
        // min_V important

        //inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);

    }


    //inRange(HSV, Scalar(H_min, S_min, V_min), Scalar(H_max, S_max, V_max), mask);
    //inRange(HSV, Scalar(18, 210, 43), Scalar(31, 256, 256), mask); // use rosbag track yellow

    // the color blue is current not perfect, coz the line on the floor and much noise
    //inRange(HSV, Scalar(100, 50, 46), Scalar(130, 255, 255), mask); // use rosbag track blue

    // for yellow (26，35，44 / 255, 44, 255)
    //namedWindow("HSV", WINDOW_AUTOSIZE);
    //imshow("HSV",mask);

    // Do image filting
    Morph_filter(mask);
    namedWindow("Morph_filter", WINDOW_AUTOSIZE);
    imshow("Morph_filter",mask);


    // before track should do filtering
    track_object(x, y, mask, Image);
    // drwa the image with x,y position, which is the object prosition in image
    namedWindow("Object Tracking", WINDOW_AUTOSIZE);
    imshow("Object Tracking",Image);


    waitKey(1);

}

bool color_callback(turtlebot::gotcolor::Request& req, turtlebot::gotcolor::Response& res)
{
    color = req.color;
    return true;
}


int main(int argc, char **argv)
{
  ros::init(argc,argv, "objectTracking");
  ros::NodeHandle nh;
  //image_transport::ImageTransport it(nh);
  //image_transport::Subscriber sub = it.subscribe("/camera/rgb/image_raw",1,imageCallback);
  ros::Subscriber compressed_sub = nh.subscribe("/camera/rgb/image_raw/compressed", 1, compressed_callback);
  pub_pose = nh.advertise<geometry_msgs::Point>("/object_pose", 1);
  ros::ServiceServer color_srv = nh.advertiseService("/got_color", color_callback);

  //yellowCallback(Image);

  ros::spin();


  return 0;
}
