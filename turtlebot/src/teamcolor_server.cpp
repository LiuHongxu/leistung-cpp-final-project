#include "ros/ros.h"
#include "turtlebot/teamcolor.h" // for Server

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

//#include "std_msgs/String.h"
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/Point.h>
#include <vector>
#include <sensor_msgs/CompressedImage.h>

using namespace std;
using namespace cv;
using namespace cv_bridge;

Mat TeamImage;


string teamdetection(Mat Yellowmask, Mat Bluemask) {
  Mat tempyellow;
  Mat tempblue;
  ROS_INFO_STREAM(__LINE__);

  Yellowmask.copyTo(tempyellow);;
  Bluemask.copyTo(tempblue);
  ROS_INFO_STREAM(__LINE__);

  vector< vector <Point> > contours_yellow;
  vector<Vec4i> hierarchy_yellow;
  ROS_INFO_STREAM(__LINE__);

  vector< vector <Point> > contours_blue;
  vector<Vec4i> hierarchy_blue;
  ROS_INFO_STREAM(__LINE__);

  findContours(tempyellow, contours_yellow, hierarchy_yellow, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
  findContours(tempblue, contours_blue, hierarchy_blue, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
  ROS_INFO_STREAM(__LINE__);


  //bool teamyellow = false;
  //bool teamblue = false;
  string collor_result;
  double area_yellow = 0;
  double area_blue = 0;
  ROS_INFO_STREAM(__LINE__);
  ROS_INFO_STREAM("yellow size" << contours_yellow.size() );
  if(contours_yellow.size() == 0)
  {
      area_yellow = 0;
  }
  else {
      for (int index = 0; index >= 0; index = hierarchy_yellow[index][0]){
        Moments moment_yellow = moments((cv::Mat)contours_yellow[index]);
        area_yellow += moment_yellow.m00;
      }
      ROS_INFO_STREAM("area_yellow: " << area_yellow);

      ROS_INFO_STREAM(__LINE__);
  }

  if(contours_blue.size() == 0)
  {
      area_blue = 0;
  }
  else {
      for (int index = 0; index >= 0; index = hierarchy_blue[index][0]){
        Moments moment_blue = moments((cv::Mat)contours_blue[index]);
        area_blue += moment_blue.m00;
      }
      ROS_INFO_STREAM("area_blue: " << area_blue);
  }
  //if (area_yellow == area_blue) {
    //ROS_INFO("Not detection prosition");
  //}
  if(area_yellow > area_blue)
  {
    collor_result = "yellow";
  }
  else{
    collor_result = "blue";
  }

  return collor_result;


}


bool teamcolor_server(turtlebot::teamcolor::Request  &req, turtlebot::teamcolor::Response &res)
{

  if (req.color_detection == true)
  {
    Mat HSV_y, HSV_b;
    Mat Bluemask;
    Mat Yellowmask;
    string col_result;
    ROS_INFO_STREAM(__LINE__);

    cvtColor(TeamImage, HSV_y, COLOR_BGR2HSV);
    HSV_b = HSV_y;

    inRange(HSV_y, Scalar(18, 110, 150), Scalar(31, 256, 256), Yellowmask);
    // use rosbag track yellow
    //inRange(HSV, Scalar(35, 210, 43), Scalar(45, 256, 256), Yellowmask); // use rosbag track yellow
    //inRange(HSV, Scalar(100, 50, 75), Scalar(150, 255, 150), Bluemask); // use rosbag track blue
    namedWindow("Morph_filter1", WINDOW_AUTOSIZE);
    imshow("Morph_filter1",Yellowmask);

    ROS_INFO_STREAM(__LINE__);

    //inRange(HSV, Scalar(125, 50, 46), Scalar(150, 255, 255), Bluemask); // use rosbag track blue
    inRange(HSV_b, Scalar(100, 50, 120), Scalar(150, 255, 200), Bluemask); // use rosbag track blue
    ROS_INFO_STREAM(__LINE__);

    namedWindow("Morph_filter2", WINDOW_AUTOSIZE);
    imshow("Morph_filter2",Bluemask);

    col_result = teamdetection(Yellowmask,Bluemask);

    ROS_INFO_STREAM(__LINE__);

    if (col_result == "blue"){
      res.str = "blue";
      ROS_INFO("blue");
    }
    else if (col_result == "yellow"){
      res.str = "yellow";
      ROS_INFO("yellow");
    }
    else {
        res.str = "unknown";
    }

  }

  waitKey(100);

  return true;
}

void imageCallback(const sensor_msgs::CompressedImageConstPtr& msg) {

    try {
        TeamImage = cv::imdecode(cv::Mat(msg->data),1);

        Rect rect(170, 200, 300, 240);

        TeamImage = TeamImage(rect);

        //ROS_INFO_STREAM("size: " << TeamImage.size);
        //namedWindow("original image", WINDOW_AUTOSIZE);
        //imshow("original image", Image);
    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("Could not convert to image");

    }



    /*
  cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

    TeamImage = cv_ptr->image;

  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
*/
}





int main(int argc, char **argv)
{
  ros::init(argc, argv, "teamcolor_server");
  ros::NodeHandle nh;
  //image_transport::ImageTransport it(nh);
  //image_transport::Subscriber sub = it.subscribe("/camera/rgb/image_raw/compressed",1,imageCallback);
  ros::Subscriber sub = nh.subscribe("/camera/rgb/image_raw/compressed",1,imageCallback);
  ros::ServiceServer service = nh.advertiseService("teamcolor_server", teamcolor_server);

  ros::spin();

  return 0;
}

