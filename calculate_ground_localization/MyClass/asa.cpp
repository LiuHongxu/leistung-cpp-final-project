else if(dis2/dis1>4.1&&dis2/dis1<5.5) ////position: + +   -  +  -   - - or - -   -  +  -   + -
{
  if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
  {
    PtsCase=3;//- -   -  +  -   + +
    cout<<"Case3:- -   -  +  -   + +"<<endl;
    a = (dis1+dis2)*2/3;
    tmpvec<<PtsCase,a,0,0;
    tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
    if(colMidindex!=disOf2PointsinTurn[0][0]){
      if(colMidindex!=disOf2PointsinTurn[1][0]){
        tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
        tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
      }
      else{
        tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
        tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
      }
    }
    else{
      if(colMidindex!=disOf2PointsinTurn[1][0]){
        tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
        tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
      }
      else{
        tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
        tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
      }
    }
    /*
    delta_X = tmp1[2]-tmp2[2];
    delta_Y = tmp1[3]-tmp2[3];
    restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,2*a,false);
    tmp3<<1,0,restoredXY[0],restoredXY[1];
    */
    resortedpc_info.push_back(tmpvec);
    resortedpc_info.push_back(tmp);
    resortedpc_info.push_back(tmp1);
    resortedpc_info.push_back(tmp2);
    resortedpc_info = sortColIdx(resortedpc_info);
    //resortedpc_info.push_back(tmp3);
  }
  else
  {
    PtsCase=4;//+ +   -  +  -   - -
    cout<<"Case4:+ +   -  +  -   - -"<<endl;
    a = (dis1+dis2)*2/3;
    tmpvec<<PtsCase,a,0,0;
    tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
    if(colMidindex!=disOf2PointsinTurn[0][0]){
      if(colMidindex!=disOf2PointsinTurn[1][0]){
        tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
        tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
      }
      else{
        tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
        tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
      }
    }
    else{
      if(colMidindex!=disOf2PointsinTurn[1][0]){
        tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
        tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
      }
      else{
        tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
        tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
      }
    }
    resortedpc_info.push_back(tmpvec);
    resortedpc_info.push_back(tmp);
    resortedpc_info.push_back(tmp1);
    resortedpc_info.push_back(tmp2);
    resortedpc_info = sortColIdx(resortedpc_info);
  }
}

else if(dis2/dis1>1&&dis2/dis1<1.2)
{
  if(dis3/dis2>0.9&& dis3/dis2<1.1){//position: + -   +  -  +   - +
    cout<<"Case5:+ -   +  -  +   - +"<<endl;
    a=dis1+dis2+dis3;
    PtsCase=5;
    tmpvec<<PtsCase,a,0,0;
    int colMid2index;
    for(int i=0;i<2;i++){
      for(int j=i+1;j<3;j++){
        if(find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j])!=-1&&find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j])!=colMidindex){
          colMid2index == find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j]);
        }
      }
    }
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid){
      tmp<<3,colMid2index,input.points[colMid2index].x,input.points[colMid2index].y;
      tmp1<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(find_same_indexwithInt(disOf2PointsinTurn[0],colMidindex)!=-1&&find_same_indexwithInt(disOf2PointsinTurn[0],colMid2index)!=-1){
        if(find_different_indexwithInt(disOf2PointsinTurn[1],colMidindex)!=-1){
          int tpidx = find_different_indexwithInt(disOf2PointsinTurn[1],colMidindex);
          tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
        }
        else{
          int tpidx = find_different_indexwithInt(disOf2PointsinTurn[1],colMid2index);
          tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
        }
      }
      else{
        if(find_different_indexwithInt(disOf2PointsinTurn[0],colMidindex)!=-1){
          int tpidx = find_different_indexwithInt(disOf2PointsinTurn[0],colMidindex);
          tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
        }
        else{
          int tpidx = find_different_indexwithInt(disOf2PointsinTurn[0],colMid2index);
          tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];

      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);
      //resortedpc_info.push_back(tmp3);
    }
    else{

      float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
      float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
      if(d1<d3){
        tmp<<1,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
        tmp1<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
      }
      else{
        tmp<<1,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
        tmp1<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
      }

      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

    }

  }

  else{
    cout<<"Case6:- -   +  +  +   - -"<<endl;
    a=dis1+dis2;
    PtsCase=6;
    tmpvec<<PtsCase,a,0,0;
    float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
    float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
    if(d1<d3){
      tmp<<3,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
      tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
    }
    else{
      tmp<<3,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
      tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
    }
    /*
    delta_X = tmp1[2]-tmp[2];
    delta_Y = tmp1[3]-tmp[3];
    restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
    tmp3<<1,0,restoredXY[0],restoredXY[1];
    Eigen::Vector2f restoredXY1;
    restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
    Eigen::Vector4f tmp4;
    tmp4<<7,0,restoredXY1[0],restoredXY1[1];
    */
    resortedpc_info.push_back(tmpvec);
    resortedpc_info.push_back(tmp);
    resortedpc_info.push_back(tmp1);
    resortedpc_info.push_back(tmp2);
    resortedpc_info = sortColIdx(resortedpc_info);

    //resortedpc_info.push_back(tmp3);
    //resortedpc_info.push_back(tmp4);
  }

}

else if(dis2/dis1>1.35&&dis2/dis1<1.6)
{
  if(dis3/dis2>0.65&& dis3/dis2<0.85){//position: - +   +  -  +   + -
    PtsCase=7;
    a=dis2;
    cout<<"Case7:- +   +  -  +   + -"<<endl;
    tmpvec<<PtsCase,a,0,0;
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
    {
      tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp1[2]-tmp2[2];
      delta_Y = tmp1[3]-tmp2[3];
      restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
      Eigen::Vector4f tmp4;
      tmp4<<7,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
    else
    {

      tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
      Eigen::Vector4f tmp4;
      tmp4<<7,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
  }


  if(dis3/dis2>0.85&& dis3/dis2<1.15){ //- +   +  -  +   - +
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
    {
      PtsCase = 8;
      a = dis2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case8:+ -   +  -  +   + -"<<endl;
      tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp1[2]-tmp2[2];
      delta_Y = tmp1[3]-tmp2[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
      tmp3<<7,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,false);
      Eigen::Vector4f tmp4;
      tmp4<<1,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
    else
    {
      PtsCase = 9;
      a = dis2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case9:- +   +  -  +   - +"<<endl;
      tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
      Eigen::Vector4f tmp4;
      tmp4<<7,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
  }



  if(dis3/dis2>1.5&& dis3/dis2<1.8){//- +   +  +  -   + -
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
    {
      PtsCase = 10;
      a = (dis1+dis2)*0.8;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case10:- +   -  +  +   + -"<<endl;
      tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
      tmp3<<7,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,false);
      Eigen::Vector4f tmp4;
      tmp4<<1,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
    else
    {
      PtsCase = 11;
      a = dis2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case11:- +   +  +  -   + -"<<endl;
      tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp1[2]-tmp2[2];
      delta_Y = tmp1[3]-tmp2[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
      Eigen::Vector4f tmp4;
      tmp4<<7,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
  }

  if(dis3/dis2>1.8&& dis3/dis2<2.2){//- +   +  +  -   - + or + -   -  +  +   + -
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
    {
      PtsCase = 12;
      a = dis1*2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case12:+ -   -  +  +   + -"<<endl;
      tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
      tmp3<<7,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,a*1.5,false);
      Eigen::Vector4f tmp4;
      tmp4<<1,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
    else
    {
      PtsCase = 13;
      a = dis2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case13:- +   +  +  -   + -"<<endl;
      tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp1[2]-tmp2[2];
      delta_Y = tmp1[3]-tmp2[3];
      restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      Eigen::Vector2f restoredXY1;
      restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
      Eigen::Vector4f tmp4;
      tmp4<<7,0,restoredXY1[0],restoredXY1[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
      //resortedpc_info.push_back(tmp4);
    }
  }
}

else if(dis2/dis1>1.9&&dis2/dis1<2.1)
{
  if(dis3/dis2>1.15 && dis3/dis2<1.35){ //+ -   +  +  -   + - or - +   -  +  +   - +
    if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
    {
      PtsCase = 14;
      a = dis1*2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case14:- +   -  +  +   - +"<<endl;
      tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp2[2]-tmp1[2];
      delta_Y = tmp2[3]-tmp1[3];
      restoredXY = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,false);
      tmp3<<1,0,restoredXY[0],restoredXY[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
    }
    else
    {
      PtsCase = 15;
      a = dis2;
      tmpvec<<PtsCase,a,0,0;
      cout<<"Case15:+ -   +  +  -   + -"<<endl;
      tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
      if(colMidindex!=disOf2PointsinTurn[0][0]){
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
          tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      else{
        if(colMidindex!=disOf2PointsinTurn[1][0]){
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
        }
        else{
          tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
          tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
        }
      }
      /*
      delta_X = tmp1[2]-tmp2[2];
      delta_Y = tmp1[3]-tmp2[3];
      restoredXY = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
      tmp3<<7,0,restoredXY[0],restoredXY[1];
      */
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);

      //resortedpc_info.push_back(tmp3);
    }
  }
}
