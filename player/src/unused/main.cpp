#include <QWidget>
#include <QApplication>
#include "../include/player/main_window.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	player::MainWindow main_window(argc,argv);
	main_window.show();
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
	return app.exec();
}
