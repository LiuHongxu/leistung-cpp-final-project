/*
Autor: Chao Xiang
TUM ID: ga62jas
group: 2
Version: 2.2
Functionen der COMMUNICATION_NODE_H:
Diese *.cpp Documente sollte die Kommunikation zwischen Referee und Turtlebot setzen.
Diese *.cpp definiert Methoden, die in der Kopfer file communication_node.h deklariert wird.
Und die Variablen und Methoden in der Klasse MainWindow invovieren, die direkte Kommunikation
mit Referee setzen.
*/
#include <ros/ros.h>
#include <string>
#include <sstream>
#include "../include/player/communication_node.h"
#include "referee.h"

CommunicationNode::CommunicationNode(int argc, char** argv, ros::NodeHandle nh):
    init_argc(argc), init_argv(argv), mainwindow(0)
{
    mainwindow = new MainWindow;
    ROS_INFO_STREAM("======Instantiierung: mainwindow von MainWindow======");
    ROS_INFO_STREAM("======Initialisierung der Variablen: flags,msgs======");
    // flag_start_stop_ = false;// Vielleicht nicht nenutzt wird
    flag_ready_reported_= false;
    flag_color_reported_= false;
    flag_abratio_reported_= false;
    start_stop_.data = false;
    init_ratio = 0.42;
    received_ratio = 0;
    //parser
    if(argc==1){
        angelina_uri = "127.0.0.1";
    }
    else if(argc==2){
        angelina_uri = argv[1];
    }
    else{
        angelina_uri = "127.0.0.1";
        ROS_INFO_STREAM("======INPUT IS WRONG======");
    }
    mainwindow->angelina_uri_ = angelina_uri;
    std::cout << angelina_uri.toUtf8().constData() << std::endl;
    ROS_INFO_STREAM("======Initialisierung: pub,sub,srv,client,timer======");
    // Publishers
    start_stop_publisher_ = nh.advertise<std_msgs::Bool>("start_stop_topic",20);
    publisher_timer_ = nh.createTimer(ros::Duration(91), &CommunicationNode::RunPubTimerCallback,this, true);
    // Services
    detection_color_server_ = nh.advertiseService("command_turtlebot",&CommunicationNode::DetermineCommands,this);
    field_size_server_ = nh.advertiseService("ab_turtlebot",&CommunicationNode::DetermineFieldSize,this);
    // Subscribers
    current_position_subscriber_ = nh.subscribe("current_position_topic",100,&CommunicationNode::ReportCurrentPositionCallback,this);
    // alive information nicht wird direct in CommunicationNode gerechnet und kein Kommunikation mit main_node gebraucht.
    // alive_status_subscriber_ = nh.subscribe("alive_status_topic",100,&CommunicationNode::ReportAliveStatusCallback,this);
    // Client
    feedback_start_client_ = nh.serviceClient<player::start>("game_start");
    ROS_INFO_STREAM("======Timing Starten======");
    mainwindow->StartAliverTimer();
    ROS_INFO_STREAM("======Mit Dem Server verbinden======");
    mainwindow->ConnectToServer(angelina_uri, 10000);
}
CommunicationNode::~CommunicationNode(){
    // falls ros sein noch verlaufen,dann schließen sie
    if(ros::isStarted())
    {
        ros::shutdown(); // Aufjedenfall gebraucht,wegen ros::start() startet
        ros::waitForShutdown();
    }
    wait();// Vielleicht nicht gebraucht,wegen CommunicationNode kein QObject Vater haben.
}
// Entnehmen den Status von Turtlebot
// Die mögliche Inputs sollte in der Type String sein. Welche als Anfrage von Turtlebot
// gesendet. Dann weiterleitet in den MainWindow. Die Response sollte von Referee
// zurückgeben, deshalb,Diese Methode wurde einige zeit brauchen.
// die zurückgebende Reponse sind auch String.
// die legale Anfrage: "ready", "yellow", "blue", "goal", "done", "gamestartready"
// die entsprechende Respose: "detection_start", "yellow", "blue", "report_goal_succeed", "report_finished_succeed"
// "wait_for_command"
bool CommunicationNode::DetermineCommands(player::command::Request &req,player::command::Response &res){
    ROS_INFO_STREAM("======Angeruft von Turtlebot in topic 'command_turtlebot'======");
    if(req.command == "ready"){
        ROS_INFO_STREAM("======'ready' Aufnehmen======");
        // int count = 0;
        // Diser Mechanisimus garantieren, wenn der Referee noch keine Reponse geben,
        // dass das Ereignis in der Zirkulation bleiben. Diser vermeidet auch die
        // ungedachtete nicht eintreten der zirkulation.
        while(!mainwindow->flag_detection_start_){
            // ROS_INFO_STREAM("======'detection_start' while zirkuliieren======");
            // ros::Duration(1).sleep();
            // count ++;
            if(!mainwindow->flag_detection_start_ && !flag_ready_reported_){
                mainwindow->ReportReady();
                flag_ready_reported_  = true;
            }
            if(mainwindow->flag_detection_start_ ){
                ROS_INFO_STREAM("====='flag_detection_start_' ist true=====");
                res.result = "detection_start";
                break;
            }
            // else if (count == 20)
            // {//vermeiden, dass Programm immer in der Zirkulation bleiben.
            // // nur maximal 20 senkunden bleiben
            //     break;
            // }
        }
    }
    else if(req.command == "yellow"||req.command == "blue"){
        ROS_INFO_STREAM("======'color' Aufnehmen======");
        if (req.command == "yellow"){
            ROS_INFO_STREAM("======'yellow' Aufnehmen======");
            mainwindow->team_color_ = yellow;
        }
        else if(req.command == "blue"){
            ROS_INFO_STREAM("======'blue' Aufnehmen======");
            mainwindow->team_color_ = blue;
        }
        else{
            ROS_INFO_STREAM("======aufgenommende Farbe wird geändert von unbekannte Mechanisimus======");
            ROS_INFO_STREAM("======'blue' als default gesendet======");
            mainwindow->team_color_ = blue;
        }
        // std::cout<<"mainwindow->team_color_ is:  "<<mainwindow->team_color_<<std::endl;
        // Glieche Mechanisimus wie in "ready"
        // int count_color = 0;
        while(!mainwindow->flag_color_){
            // ROS_INFO_STREAM("======'report Color' while zirkuliieren======");
            // ros::Duration(1).sleep();
            // count_color ++;
            if(!mainwindow->flag_color_ && !flag_color_reported_){
                mainwindow->ReportColor(mainwindow->team_color_);
                flag_color_reported_ = true;
            }
            if(mainwindow->flag_color_){
                ROS_INFO_STREAM("======zurückgebende tatsächlische Farbe ======");
                std::string true_color = mainwindow->color_string_;
                res.result = true_color;
                break;
            }
            // else if(count_color == 20)
            // {
                // break;
            // }
        }
    }
    else if(req.command == "goal"){
        ROS_INFO_STREAM("======'goal' Aufnehmen======");
        mainwindow->ReportGoal();
        res.result = "report_goal_succeed";
    }
    else if(req.command == "done"){
        ROS_INFO_STREAM("======'done' Aufnehmen======");
        mainwindow->ReportDone();
        res.result = "report_finished_succeed";
    }
    // else if(req.command == "gamestartready"){
    //     ROS_INFO_STREAM("======ready for start the game======");
    //     res.result = "wait_for_command";
    //     this->PublishStartStopCommand(true);
    // }
    else{
        ROS_INFO_STREAM("======illegale Nachrichte Aufnehmen======");
        res.result = "wait_for_command";
    }
    return true;
}
// Entnehmen die Größe des Spielplatz von Turtlebot
// Die mögliche Inputs sollte in der Type double sein. Welche als Anfrage von Turtlebot
// gesendet. Dann weiterleitet die Propotion in den MainWindow. Die Response sollte von Referee
// zurückgeben, deshalb,Diese Methode wurde einige zeit brauchen.
// die zurückgebende Reponse sind auch double.
// die legale Anfrage: "a", "b"
// die entsprechende Respose: "a_true", "b_true"
bool CommunicationNode::DetermineFieldSize(player::a_and_b::Request &req, player::a_and_b::Response &res){
    ROS_INFO_STREAM("======'Spielplatz Größe' Aufnehmen======");
    received_ratio = (req.a)/(req.b);
    if(fabs(received_ratio - init_ratio) > 0.05){
        received_ratio = init_ratio;
    }
    else{
        received_ratio = received_ratio;
    }
    while(!mainwindow->flag_field_size_){
        // ROS_INFO_STREAM("======'feedback field size while' Zirkulation======");
        // ros::Duration(1).sleep();
        if(!mainwindow->flag_field_size_ && !flag_abratio_reported_){
            mainwindow->ReportAbRatio(received_ratio);
            flag_abratio_reported_ = true;
        }
        if(mainwindow->flag_field_size_){
            // std::cout<<"mainwindow->a_true_ is:  "<<mainwindow->a_true_<<std::endl;
            // std::cout<<"mainwindow->b_true_ is:  "<<mainwindow->b_true_<<std::endl;
            res.a_true = mainwindow->a_true_;
            res.b_true = mainwindow->b_true_;
            break;
        }
    }
    return true;
}
//Callback Function für den Subscriber der akutuelle position
void CommunicationNode::ReportCurrentPositionCallback(const geometry_msgs::PointConstPtr &current_position){
    mainwindow->UpdatePos(current_position->x,current_position->y);
}
// void CommunicationNode::ReportAliveStatusCallback(const std_msgs::BoolConstPtr &isalived){}
// Publisher, welche die Befehle "game_start", "resume", "stop" und "game_over" zusammen als ein
// bool Variablen "flag_start_stop" setzen. Wenn "flag_start_stop" true erstmal true ist, wird das
// Spiel gestartet. Sonst wird der Roboter nicht bewegung nehmen.
bool CommunicationNode::PublishStartStopCommand(bool flag_start_stop){
    ROS_INFO_STREAM("======Anfang von der publisher ======");
    //Zuerst anrufen, falls Turtlebot für game_start bereit ist. und mittelen, dass
    // das Spiel starten kann.
    player::start srv;
    srv.request.start = true;
    ROS_INFO_STREAM("======Rückkopplung start_flag von Turtlebot darf game_start======");
    this->feedback_start_client_.call(srv);
    ros::Rate rate(1);// senden, Bewegung Status in 1 Hz
    if (!mainwindow->flag_game_start_ && !mainwindow->flag_stop_){
        mainwindow->flag_stop_ = true;
    }
    while(mainwindow->flag_game_start_ || mainwindow->flag_stop_){
        ROS_INFO_STREAM("======Die genaue Status rückkoppeln======");
        if (mainwindow->flag_stop_){
            start_stop_.data = false;
        }
        else{
            start_stop_.data = true;
        }
        this->start_stop_publisher_.publish(start_stop_);
        rate.sleep();
    }
    return true;
}
// Wenn seit detection_start 90s erreicht, wird der Timer anrufen, die Signale game_start
// zu merken.
void CommunicationNode::RunPubTimerCallback(const ros::TimerEvent &event){
    ROS_INFO_STREAM("======91s erreicht, wird start_======");
    this->PublishStartStopCommand(true);
}
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
    ros::init(argc,argv,"ros_qt_communication");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4);
    spinner.start();
    CommunicationNode communication_node(argc, argv, nh);
    int result = app.exec();
    ros::waitForShutdown();
    return result;
}
