#include <QMessageBox>
#include <iostream>
#include <QDebug>
#include "../include/player/main_window.h"
#include "referee.h"

MainWindow::MainWindow(QObject *parent):
    QObject(parent), referee_(0)
// MainWindow::MainWindow():referee_(0)
{
    // initialize the flags, commands ,position and color
    flag_detection_start_ = false;
    flag_color_ = false;
    flag_field_size_ = false;
    flag_game_start_ = false;
    flag_stop_ = false;
    angelina_uri_ = "127.0.0.1";
    color_string_ = "yellow";
    team_color_ = yellow;
    a_true_ = 4;
    b_true_ = 2.25;
    qRegisterMetaType<TeamColor>("TeamColor");
    referee_ = new Referee(2,this);
    referee_->moveToThread(&MainWindowThead);
    connect(&MainWindowThead, &QThread::finished, referee_, &QObject::deleteLater);
    std::cout << "======Set connection between MainWindow and Referee======" << std::endl;
    //connection between MainWindow and Referee
    //=====worked well at home,signals to the referee=====
    connect(this,SIGNAL(signalConnectToServer(const QString &, int)),referee_,SLOT(slotConnectToServer(const QString &, int)));
    connect(this,SIGNAL(signalReportReady()),referee_,SLOT(slotReportReady()));
    connect(this,SIGNAL(signalSendAlive()),referee_,SLOT(slotSendAlive()));
    connect(this,SIGNAL(signalReportDone()),referee_,SLOT(slotReportDone()));
    connect(this,SIGNAL(signalReportAbRatio(double)),referee_,SLOT(slotTellAbRatio(double)));
    connect(this,SIGNAL(signalTellTeamColor(TeamColor)),referee_,SLOT(slotTellTeamColor(TeamColor)));
    connect(this,SIGNAL(signalReportGoal()),referee_,SLOT(slotReportGoal()));
    connect(this,SIGNAL(signalTellEgoPos(double , double)),referee_,SLOT(slotTellEgoPos(double , double)));

    connect(referee_, SIGNAL(detectionStart()), this, SLOT(SlotDetectionStart()));
    connect(referee_, SIGNAL(trueColorOfTeam(TeamColor)), this, SLOT(SlotFeedbackTrueColor(TeamColor)));
    connect(referee_, SIGNAL(abValues(double,double)), this, SLOT(SlotAbValues(double,double)));
    connect(referee_, SIGNAL(gameStart()), this, SLOT(SlotGameStart()));
    connect(referee_, SIGNAL(stopMovement()), this, SLOT(SlotStopMovement()));
    connect(referee_, SIGNAL(gameOver()), this, SLOT(SlotGameOver()));//

    MainWindowThead.start();
}
MainWindow::~MainWindow() {
    delete referee_;
    MainWindowThead.quit();
    MainWindowThead.wait();
}
// ================================slots for MainWindow=========================
void MainWindow::SlotDetectionStart(){
    std::cout << "======Feedback: DetectionStart======" << std::endl;
    std::cout << "======Set DetectionStart Flag as true======" << std::endl;
    this->flag_detection_start_ = true;
}
void MainWindow::SlotFeedbackTrueColor(TeamColor team_color){
    std::cout << "======Feedback: True Color======" << std::endl;
    switch (team_color) {
        case(yellow):{
            this->color_string_ = "yellow";
            std::cout <<"true color should be: "<<this->color_string_<<std::endl;
            break;
        }
        case(blue):{
            this->color_string_ = "blue";
            std::cout <<"true color should be: "<<this->color_string_<<std::endl;
            break;
        }
    }
    std::cout <<"set flag_color_ as true"<<std::endl;
    this->flag_color_ = true;
}
void MainWindow::SlotAbValues(double a_true, double b_true){
    std::cout << "======FEEDBACK: True Field Size======" << std::endl;
    this->a_true_ = a_true;
    this->b_true_ = b_true;
    this->flag_field_size_ = true;
    std::cout <<"a_true_ = "<<a_true_<<std::endl;
    std::cout <<"b_true_ = "<<b_true_<<std::endl;
    std::cout <<"flag_field_size_ = "<<flag_field_size_<<std::endl;
}
void MainWindow::SlotSendAlive(){
    std::cout << "======send alive, per 20 senconds======" << std::endl;
    if (referee_->isConnected()){
        std::cout << "======akutuelle Status ist verbunden======" << std::endl;
        Q_EMIT signalSendAlive();
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::SlotGameStart(){
    std::cout << "======GameStart signal from Angelina======" << std::endl;
    if (referee_->isConnected()){
        std::cout << "======akutuelle Status ist verbunden======" << std::endl;
        //do some work to make the automatic publish possible
        flag_game_start_ = true;
        flag_stop_ = false;
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::SlotStopMovement(){
    std::cout << "======Stop move signal from Angelina======" << std::endl;
    if (referee_->isConnected()){
        std::cout << "======akutuelle Status ist verbunden======" << std::endl;
        //do some work to make the automatic publish possible
        flag_game_start_ = false;
        flag_stop_ = true;
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::SlotGameOver(){
    std::cout << "======Gameover/Stop signal from Angelina======" << std::endl;
    if (referee_->isConnected()){
        std::cout << "======akutuelle Status ist verbunden======" << std::endl;
        //do some work to make the automatic publish possible
        flag_game_start_ = false;
        flag_stop_ = true;
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
//===========================methods for MainWindow=============================
//===========================Q_EMIT Signals to Referee==========================
void MainWindow::StartAliverTimer(){
    QTimer *aliveTimer = new QTimer(this);
    connect(aliveTimer, SIGNAL(timeout()), this, SLOT(SlotSendAlive()));
    aliveTimer->start(20000);// send aliver information per 20 senconds
}
void MainWindow::ConnectToServer(const QString &ip, int port){
    std::cout << "Connecting to server..." << std::endl;
    if (!referee_->isConnected()){
        std::cout << "=======connecting...=======" << std::endl;
        Q_EMIT signalConnectToServer(ip, port);
        std::cout << "=======send signals=======" << std::endl;
    }
    else{
        this->flag_detection_start_ = false;
    }
}
void MainWindow::ReportReady(){
    std::cout << "Report Turtlebot is Ready for game" << std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalReportReady();
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::ReportColor(TeamColor team_color){
    std::cout << "Report detected color " <<team_color<< std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalTellTeamColor(team_color);
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::UpdatePos(double x, double y){
    std::cout << "Update current position" << std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalTellEgoPos(x,y);
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::ReportGoal(){
    std::cout << "Report the robot is goaled" << std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalReportGoal();
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::ReportDone(){
    std::cout << "Report game is done by us" << std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalReportDone();
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
void MainWindow::ReportAbRatio(double ratio){
    std::cout << "Report the filed size in artio of a/b" << std::endl;
    if (referee_->isConnected()){
        std::cout << "akutuelle Status ist verbunden" << std::endl;
        Q_EMIT signalReportAbRatio(ratio);
    }
    else{
        this->ConnectToServer(angelina_uri_, 10000);
        this->flag_detection_start_ = false;
    }
}
