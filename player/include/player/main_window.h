#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QTimer>
#include <QWidget> //this is necessary for window
#include <QObject>
#include <QThread>
#include <QMap>
#include <QMetaType>
#include "referee.h"//copy from the example in the Angelina


class MainWindow :public QObject
{
    Q_OBJECT
    QThread MainWindowThead;
public:
    MainWindow(QObject *parent = 0);
    ~MainWindow();
    // communicate with referee, then we need a instance of Referee
    Referee *referee_;

    // flags to send the Commands
    bool flag_detection_start_;
    bool flag_color_;
    bool flag_field_size_;
    bool flag_game_start_;
    bool flag_stop_;
    QString angelina_uri_;
    // variables to save the color and filed_size
    TeamColor team_color_;
    std::string color_string_;
    double a_true_;
    double b_true_;

    // methods for MAIN_WINDOW_H
    void StartAliverTimer();
    void ConnectToServer(const QString &ip, int port);
    void ReportReady();
    void ReportColor(TeamColor team_color);
    void UpdatePos(double x, double y);//can substitute as a normal functions
    void ReportDone();
    void ReportAbRatio(double ratio);
    void ReportGoal();
Q_SIGNALS:
    void signalConnectToServer(const QString &, int);
    void signalReportReady();
    void signalReportDone();
    void signalSendAlive();
    void signalReportAbRatio(double);
    void signalTellTeamColor(TeamColor);//stop use this as tempora
    void signalReportGoal();
    void signalTellEgoPos(double , double);

public Q_SLOTS:
    void SlotDetectionStart();
    void SlotFeedbackTrueColor(TeamColor);
    void SlotSendAlive();
    void SlotAbValues(double,double);
    void SlotGameStart();
    void SlotStopMovement();
    void SlotGameOver();
};
#endif /* MAIN_WINDOW_H */
