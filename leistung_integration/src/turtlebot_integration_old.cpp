#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <math.h>
#include <std_msgs/Bool.h>
#include <algorithm>
//include srv header
//#include <leistung_integration/localization.h>
//#include <leistung_integration/moveForL.h>
//#include <leistung_integration/angleRotation.h>

#include <calculate_ground_localization/ground_info.h>
#include <leistung_integration/a_and_b.h>
#include <leistung_integration/command.h>
#include <leistung_integration/forward.h>
#include <leistung_integration/rotation.h>
#include <leistung_integration/start.h>
//#include <leistung_integration/back.h>
//#include <leistung_integration/initial_loc.h>
#include <plane_segmentation/initial_Loc.h>
#include <leistung_integration/rotation_test.h>

#include <turtlebot/teamcolor.h>
#include <turtlebot/gotcolor.h>

//
#include <string>
#include <vector>
#include <map>
#include <iomanip>

using namespace std;

class integration
{
private:
    ros::NodeHandle nh_, priv_nh_;
    ros::ServiceClient command_cli, ab_cli, iniLoc_cli, localization_cli;//, angleRotation_cli;
    ros::ServiceClient groupColor_cli, rotation_cli, forward_cli, gotColor_cli;
    ros::ServiceClient rotation_test_cli;
    ros::Subscriber odom_sub, status_sub;
    ros::ServiceServer start_serv;
    ros::Publisher vel_pub, position_pub, alive_pub;

    //-------parameters----------------//
    double a, b, x_d, y_d, x_, y_, x, y, theta;
    double x_t, y_t, theta_t;
    std::string place, team_color;
    bool get_b, get_color, get_bothL, status, start, finish, end, first_move;
    double i;
    double theta_0, deta_theta, dis_0, DIS, theta_cur;
    double sin_phi2, phi;
    double x_cur, y_cur, desired_d, x_odm, y_odm;
    int k, time, initmove_times;
    double alpha, beta;
    //enum
    enum state_set {ready, initail_Loc,  localization, moveForTeamcolor, group_color, waitingForStart, go_central, rotation, forward, to_conor, find_goal,  go_goal, go_back, wait};
    state_set state;
    //----service parameters----//
    plane_segmentation::initial_Loc iniLoc_srv;
    //leistung_integration::initial_loc iniLoc_srv;
    leistung_integration::rotation rotation_srv;
    leistung_integration::forward forward_srv;
    leistung_integration::command command_srv;
    //leistung_integration::back back_srv;
    leistung_integration::a_and_b a_b_srv;
    //leistung_integration::localization localization_srv;
    leistung_integration::rotation_test rotationTest_srv;
    //leistung_integration::angleRotation angleRotation_srv;
    calculate_ground_localization::ground_info localization_srv;
    turtlebot::teamcolor team_color_srv;
    turtlebot::gotcolor got_color;
    geometry_msgs::Twist vel_msg;
    geometry_msgs::Point cur_position;
    std_msgs::Bool alive;
    //-----service client------//
    //-----callback---//
    void odomCallback(const nav_msgs::OdometryConstPtr& odom_msg);
    void statusCallback(const std_msgs::Bool& status_msg);
    bool startCallback(leistung_integration::start::Request &req, leistung_integration::start::Response &res);

public:
    integration(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
        /*
        a = 1.2;
        b = 3.0;
        x_d = 0.9;//a * 0.375;
        y_d = 0.1;//0.8 * b /3;
        x_ = 0.0;
        y_ = 0.0;
        x = 0.0;
        y = 0.0;
        theta = 0;
        theta_t = 0;//- 3.1416;
        */


        time = 0;
        initmove_times = 0;
        //get_b = false;

        get_b = false;
        place = "unknown";
        //x_t = 1.5;
        //y_t = 0.6;
        get_color = false;
        get_bothL = false;
        start = false;
        first_move = false;
        i = 0.0;
        dis_0 = 0;
        DIS = 0;
        theta_0 = 0;
        //place = "unknown";
        team_color = "unknown";
        ros::Rate loop_rate(0.1);
        ros::Rate l_rate(1);
        // wait for theaction server to come up
        //---------service client-------------------//
        command_cli = nh_.serviceClient<leistung_integration::command>("/command_turtlebot");
        ab_cli = nh_.serviceClient<leistung_integration::a_and_b>("/ab_turtlebot");
        iniLoc_cli = nh_.serviceClient<plane_segmentation::initial_Loc>("/initial_Loc");
        localization_cli = nh_.serviceClient<calculate_ground_localization::ground_info>("/InitialPosition");
        //angleRotation_cli = nh_.serviceClient<leistung_integration::angleRotation>("/angleRotation_turtlebot");
        groupColor_cli = nh_.serviceClient<turtlebot::teamcolor>("/teamcolor_server");
        rotation_cli = nh_.serviceClient<leistung_integration::rotation>("/rotation_turtlebot");
        forward_cli = nh_.serviceClient<leistung_integration::forward>("/forward_turtlebot");
        gotColor_cli = nh_.serviceClient<turtlebot::gotcolor>("/got_color");
        rotation_test_cli = nh_.serviceClient<leistung_integration::rotation_test>("/rotation_turtlebot_test");

        //goBack_cli = nh_.serviceClient<leistung_integration::back>("/back_turtlebot");
        //----------------subscriber-----------------//
        odom_sub = nh_.subscribe("/odom", 1, &integration::odomCallback, this);
        status_sub = nh_.subscribe("/status", 1, &integration::statusCallback, this);
        //------------------service server-------------------//
        start_serv = nh_.advertiseService("/game_start", &integration::startCallback, this);
        //-----------------publisher----------------------//
        vel_pub = nh_.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);//turtle1/cmd_vel
        position_pub = nh_.advertise<geometry_msgs::Point>("/current_position_topic", 1);
        alive_pub = nh_.advertise<std_msgs::Bool>("/alive_status_topic", 1);

        state = initail_Loc;
        ros::Duration(2).sleep();
        end = false;

        while(ros::ok() && !end)
        {

            switch(state){
            // send ready to angelinna
            case ready:
                command_srv.request.command = "ready";
                if(command_cli.call(command_srv))
                {
                    if(command_srv.response.result == "ready")
                    {
                        ROS_INFO("Get ready command back !!!");
                        state = initail_Loc;
                        break;//go to localization state;
                    }
                }
                break;//still in ready state

            // prepare for localization make sure both eghes could be found by turtlebot
            case initail_Loc:
                iniLoc_srv.request.localization.data = true;
                for( k = 0; k <= 5 ; k++ )
                {
                    iniLoc_cli.call(iniLoc_srv);
                    while(iniLoc_srv.response.dis.data <= 0.0)//didn't find anything
                    {
                        iniLoc_cli.call(iniLoc_srv);
                    }

                    //ROS_INFO_STREAM("k: " << k);
                    if( k == 5 )
                    {
                        iniLoc_srv.request.localization.data = false;
                        iniLoc_cli.call(iniLoc_srv);

                    }
                    else if(iniLoc_srv.response.lines.data)
                    {
                        k = 12;
                        get_bothL = true;
                        theta_0 = iniLoc_srv.response.theta.data;
                        dis_0 = iniLoc_srv.response.dis.data;
                        ROS_INFO("FIND TWO LINES");
                        ROS_INFO_STREAM( "dis: " << dis_0 );
                        ROS_INFO_STREAM( "theta: " << theta_0 );
                    }
                    else// just found one line
                    {

                        theta_0 = iniLoc_srv.response.theta.data;
                        dis_0 = iniLoc_srv.response.dis.data;

                        get_bothL = false;
                        ROS_INFO("JUST FIND ONE LINE");
                        ROS_INFO_STREAM("dis: " << dis_0 );
                        ROS_INFO_STREAM("theta: " << theta_0 );
                        ROS_INFO_STREAM(__LINE__);
                    }
		    ROS_INFO_STREAM("both lines: " << iniLoc_srv.response.lines.data);
                    l_rate.sleep();
                }

                if( DIS <= 0 )
                {
                    DIS = dis_0;
                }

                if(get_bothL)
                {
                    state = localization;
                    ROS_INFO("Go to localization case");
                    ROS_INFO_STREAM(__LINE__);
                    break;
                }
                else
                {
                    if( dis_0 >= 0.95 )
                    {
                        ROS_INFO("Don't need to move further");
                        //dont need to go further but need to check the angular
                        if( 0.5 < fabs(theta_0) < 1.0)
                        {
                            ROS_INFO("Don't need to turn anymore");
                            state = initail_Loc;
                            break;
                        }
                        else
                        {
                            rotationTest_srv.request.angle = theta_0 / fabs(theta_0) * ( fabs(theta_0) - 0.75);
                            if(rotation_test_cli.call(rotationTest_srv))
                            {
                                state = initail_Loc;
                                break;
                            }
                        }
                    }
                    else if( !first_move ) // need to move
                    {
		        ROS_INFO_STREAM("dis_0" << dis_0 << "DIS " << DIS);
                        ROS_INFO_STREAM(__LINE__);
                        rotationTest_srv.request.angle = theta_0;
                        if(rotation_test_cli.call(rotationTest_srv))//turn theta_0
                        {

                            if(rotationTest_srv.response.done)
                            {
                                forward_srv.request.distance = 1.5 - dis_0;//go further
                                ROS_INFO("Need to move further");
                                forward_cli.call(forward_srv);

                                rotationTest_srv.request.angle = 0.7;
                                if(rotation_test_cli.call(rotationTest_srv))// turn for
                                {
                                    state = initail_Loc;
                                    first_move = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                         
                         state = initail_Loc;
                         break;
                    }

                }


                //theta_0 = iniLoc_srv.response.theta.data;
                //dis_0 = iniLoc_srv.response.dis.data;

                //ROS_INFO_STREAM("k: " << k);
                //ROS_INFO_STREAM(__LINE__);

            case localization:
                ros::spinOnce();
                ROS_INFO_STREAM(__LINE__);
                //localiztion server with response a, b, x0, y0, theta, place(left, right)
                localization_srv.request.start = true;
                if(localization_cli.call(localization_srv))
                {
                    a = localization_srv.response.a;
                    if(localization_srv.response.b != 0.0)
                    {
                        get_b = true;
                        ROS_INFO("find two lines");
                        a = localization_srv.response.a;
                        b = localization_srv.response.b;

                        /*
                        a_b_srv.request.a = localization_srv.response.a;
                        a_b_srv.request.b = localization_srv.response.b;


                        if(ab_cli.call(a_b_srv))
                        {
                            a = a_b_srv.response.a_true;
                            b = a_b_srv.response.b_true;
                        }
                    */
                    }
                    else {
                        get_b = false;
                    }
                }
                x_ = localization_srv.response.x_;
                y_ = localization_srv.response.y_;
                theta = localization_srv.response.theta;
                place = localization_srv.response.place;
                ROS_INFO_STREAM(__LINE__);
                ROS_INFO_STREAM("x_ : " << x_ );
                ROS_INFO_STREAM("y_ : " << y_ );
                ROS_INFO_STREAM("theta: " << theta );
                ROS_INFO_STREAM("place: " << place );
                ROS_INFO_STREAM("a: " << a );
                ROS_INFO_STREAM("b: " << b );
                ROS_INFO_STREAM(__LINE__);

                if(get_b && time == 0)
                {

		    if((x_ >= 0.5 *b && place == "left") || (x_ <= 0.5 * b && place == "right" ))
		    {
		        x_t = DIS;
		    }
                    else
                    {
                        x_t = b - DIS;
			
                    }

                    y_t = y_;
                    theta_t = theta - phi +  1.5708; //theta_t if from odom to map
                    ros::spinOnce();


                    ROS_INFO_STREAM("x_t: " << x_t);
                    ROS_INFO_STREAM("y_t: " << y_t);
                    ROS_INFO_STREAM("theta_t: " << theta_t);

                    ROS_INFO_STREAM(__LINE__);
                    time = 1;
                    state = moveForTeamcolor;

                    break;
                }
                else {
                    state = localization;
                    break;
                }

            case moveForTeamcolor:
                ros::spinOnce();
		ROS_INFO_STREAM("go to find team color");
                

		if( place == "right")
                {
                    x_d = b / 3 - 0.2;
                }
                else
                {
                    x_d = 2 * b / 3 + 0.2;
                }
                y_d = a * 0.375;
                ROS_INFO_STREAM("x_cur: " << x << "y_cur " << y << "phi " << phi);
		ROS_INFO_STREAM("x_d: " << x_d << "y_d" << y_d);

                beta = atan(fabs((y-y_d)/(x-x_d)));
                alpha = (x-x_d)/fabs(x-x_d)*M_PI/2 + ((x-x_d)/fabs(x-x_d) * (y-y_d)/fabs(y-y_d)) * beta;
                
		theta_cur = theta_t - 1.5708 + phi;
                deta_theta = alpha - theta_cur;
                ROS_INFO_STREAM("alpha: " << alpha << "theta_cur" << theta_cur);
                rotationTest_srv.request.angle = deta_theta;

                if(rotation_test_cli.call(rotationTest_srv))
                {
                    ros::spinOnce();
                    ROS_INFO_STREAM("rotation to teamcolor 1");

		    if(rotationTest_srv.response.done)
                    {
                        x_cur = x;
                        y_cur = y;
                        desired_d =  sqrt((x_d - x_cur)*(x_d - x_cur) + (y_d - y_cur)*(y_d -y_cur));
                        

			forward_srv.request.distance = desired_d;
			ROS_INFO_STREAM("desired_d: " << desired_d);
                        forward_srv.request.rotation = false;
                        if(forward_cli.call(forward_srv))
                        {
                            ROS_INFO_STREAM("forward to teamcolor");
                            if(forward_srv.response.done)
                            {
		                if( place == "right")
                                {
                                    deta_theta = -1.5708 - alpha;
                                }
                                else
                                {
                                    deta_theta = +1.5708 - alpha;
                                }

                                rotationTest_srv.request.angle = deta_theta;
                                if(rotation_test_cli.call(rotationTest_srv))
                                {
                                    ROS_INFO_STREAM("rotation to teamcolor 2");
                                    state = group_color;
                                    break;
                                }

                            }
                        }
                    }
                }
                else
                {
                    state = moveForTeamcolor;
                    break;
                }

            case group_color:
                team_color_srv.request.color_detection = true;
		ros::Duration(3).sleep();
                ROS_INFO_STREAM("group color case: ");                    
                if(groupColor_cli.call(team_color_srv))
                {
		    team_color = team_color_srv.response.str;
		    ROS_INFO_STREAM("team_color: " << team_color);                    
                    got_color.request.color = team_color;                    
                    gotColor_cli.call(got_color);

//gotColor_cli.call(got_color);

/*

                    command_srv.request.command = team_color_srv.response.str;
                    if(command_cli.call(command_srv))
                    {
                        team_color = command_srv.response.result;
                        got_color.request.color = team_color;
                        gotColor_cli.call(got_color);

                    }
*/
                    state = go_central;
                    break;
                }
                else {
                    state = group_color;
                    break;
                }

            case go_central:
                ros::spinOnce();
		ROS_INFO_STREAM("go to find team color");
                
                x_d = b / 2.0;
                y_d = a * 0.5 + 0.3;

                beta = atan(fabs((y-y_d)/(x-x_d)));
                alpha = (x-x_d)/fabs(x-x_d)*M_PI/2 + ((x-x_d)/fabs(x-x_d) * (y-y_d)/fabs(y-y_d)) * beta;
                
		theta_cur = theta_t - 1.5708 + phi;
                deta_theta = alpha - theta_cur;
                ROS_INFO_STREAM("alpha: " << alpha << "theta_cur" << theta_cur);
                rotationTest_srv.request.angle = deta_theta;

                if(rotation_test_cli.call(rotationTest_srv))
                {
                    ros::spinOnce();
                    ROS_INFO_STREAM("rotation to center 1");

		    if(rotationTest_srv.response.done)
                    {
                        x_cur = x;
                        y_cur = y;
                        desired_d =  sqrt((x_d - x_cur)*(x_d - x_cur) + (y_d - y_cur)*(y_d -y_cur));
                        

			forward_srv.request.distance = desired_d;
			ROS_INFO_STREAM("desired_d: " << desired_d);
                        forward_srv.request.rotation = false;
                        if(forward_cli.call(forward_srv))
                        {
                            ROS_INFO_STREAM("forward to center");
                            if(forward_srv.response.done)
                            {
                                deta_theta = - alpha;
                                rotationTest_srv.request.angle = deta_theta;
                                if(rotation_test_cli.call(rotationTest_srv))
                                {
                                    ROS_INFO_STREAM("rotation to center 2");
                                    state = rotation;//waitingForStart;
                                    break;
                                }

                            }
                        }
                    }
                }
                else
                {
                    state = go_central;
                    break;
                }

            case waitingForStart:
                if(start)
                {
                    state = rotation;
                    break;
                }
                else {
                    state = waitingForStart;
                    break;
                }

            case rotation:
                ROS_INFO_STREAM("case rotation");
              rotation_srv.request.start = true;
              rotation_cli.call(rotation_srv);
              if (rotation_srv.response.find == true)
              {
                ROS_INFO("Find the object directly in front !");
                if(rotation_srv.response.distance_ > 0)
                {
                    forward_srv.request.distance = rotation_srv.response.distance_;
                    forward_srv.request.rotation = true;
                    state = forward;
                    break;
                }
              }

            case forward:
                ROS_INFO_STREAM("case forward");

                ROS_INFO_STREAM("distance " << forward_srv.request.distance);
                forward_cli.call(forward_srv);
                if(forward_srv.response.done == true)
                {
                  ROS_INFO("Get the object !");
                  state = find_goal;
                  break;
                }


            case find_goal:
                ros::spinOnce();
                x_d = b / 2.0 ;//+ i * 0.1;
                y_d = a * 2.3 + i * 0.03;
                if(x > x_d)
                {
                    x_d = x_d + 0.4 + i * 0.1;
                }
                else if (x < x_d)
                {
                    x_d = x_d - 0.2 - i * 0.1;
                }
                beta = atan(fabs((y-y_d)/(x-x_d)));
                alpha = (x-x_d)/fabs(x-x_d)*M_PI/2 + ((x-x_d)/fabs(x-x_d) * (y-y_d)/fabs(y-y_d)) * beta;

                theta_cur = theta_t - 1.5708 + phi;

                deta_theta = alpha - theta_cur;

                rotationTest_srv.request.angle = deta_theta;
                //ROS_INFO_STREAM("x: " << x << "y: " << y);
                //ROS_INFO_STREAM("deta: " << deta_theta);

                if(rotation_test_cli.call(rotationTest_srv))
                {
                    if(rotation_srv.response.find)
                    {
                        ROS_INFO("find goal !");

                        state = go_goal;
                        //end = true;
                        break;//go to go_goal state
                    }

                }
                break;//still in find_goal state
            case go_goal:
                ros::spinOnce();
                x_cur = x;
                y_cur = y;
                x_d = b / 2.0 ;//+ i * 0.1;
                y_d = a * 2.3;
                desired_d =  sqrt((x_d - x)*(x_d - x) + (y_d - y)*(y_d - y));
                forward_srv.request.distance = desired_d;
                forward_srv.request.rotation = false;
                //ROS_INFO_STREAM("distance: " << desired_d);


                if(forward_cli.call(forward_srv))
                {
                    if(forward_srv.response.done)
                    {
                        deta_theta = - alpha;
                        rotationTest_srv.request.angle = deta_theta;

                        if(rotation_test_cli.call(rotationTest_srv))
                        {
                            if(rotation_srv.response.find)
                            {
                                state = go_back;
                                break;//go to go_back state
                            }
                        }
                    }

                }
                break;

            case go_back:

                forward_srv.request.distance = -0.2;
                forward_srv.request.rotation = false;
                if(forward_cli.call(forward_srv))
                {
                    if(forward_srv.response.done == true)
                    {
                        rotationTest_srv.request.angle = 1.571;

                        if(rotation_test_cli.call(rotationTest_srv))
                        {
                            if(rotation_srv.response.find)
                            {
                                ROS_INFO("find goal !");
                                if ( i < 2.0)
                                {
				    ROS_INFO_STREAM("the robot : " << i);
                                    state = to_conor;
				    i++;
                                    break;
                                }
                                else
                                {
				    ROS_INFO_STREAM("the robot : " << i);
                                    state = wait;
                                    //end = true;
                                
                                    break;
                                }
                            }

                        }
                    }
                }



            case to_conor:

                ros::spinOnce();
		ROS_INFO_STREAM("go to find team color");
                
                x_d = b / 6.0;
                y_d = a * 2.3 - 0.2;

                beta = atan(fabs((y-y_d)/(x-x_d)));
                alpha = (x-x_d)/fabs(x-x_d)*M_PI/2 + ((x-x_d)/fabs(x-x_d) * (y-y_d)/fabs(y-y_d)) * beta;
                
		theta_cur = theta_t - 1.5708 + phi;
                deta_theta = alpha - theta_cur;
                ROS_INFO_STREAM("alpha: " << alpha << "theta_cur" << theta_cur);
                rotationTest_srv.request.angle = deta_theta;

                if(rotation_test_cli.call(rotationTest_srv))
                {
                    ros::spinOnce();
                    ROS_INFO_STREAM("rotation to center 1");

		    if(rotationTest_srv.response.done)
                    {
                        x_cur = x;
                        y_cur = y;
                        desired_d =  sqrt((x_d - x_cur)*(x_d - x_cur) + (y_d - y_cur)*(y_d -y_cur));
                        

			forward_srv.request.distance = desired_d;
			ROS_INFO_STREAM("desired_d: " << desired_d);
                        forward_srv.request.rotation = false;
                        if(forward_cli.call(forward_srv))
                        {
                            ROS_INFO_STREAM("forward to center");
                            if(forward_srv.response.done)
                            {
                                deta_theta = - alpha + 3.14;
                                rotationTest_srv.request.angle = deta_theta;
                                if(rotation_test_cli.call(rotationTest_srv))
                                {
                                    ROS_INFO_STREAM("rotation to center 2");
                                    
			                        forward_srv.request.distance = - 0.4;
                                    forward_srv.request.rotation = false;
                                    if(forward_cli.call(forward_srv))
                                    {
					                    state = rotation;//waitingForStart;
                                	    break;
                                    }

                                }

                            }
                        }
                    }
                }
                else
                {
                    state = to_conor;
                    break;
                }

            case wait:
                ros::spinOnce();
                //ROS_INFO_STREAM("x: " << x << "y: " << y);
                ROS_INFO("Waiting !");
                state = wait;
                break;
             //default rotation;
            }
            ros::spinOnce();
            loop_rate.sleep();
        }
    }
    ~integration(){}
};


void integration::odomCallback(const nav_msgs::OdometryConstPtr& odom_msg)
{

    sin_phi2 = odom_msg->pose.pose.orientation.z;
    phi = 2 * asin(sin_phi2);//   -pi, +pi
    x_odm = odom_msg->pose.pose.position.x;
    y_odm = odom_msg->pose.pose.position.y;
    if(get_b && (place != "unknown"))
    {
        x = (odom_msg->pose.pose.position.x) * cos(theta_t) - (odom_msg->pose.pose.position.y) * sin(theta_t) + x_t;
        y = (odom_msg->pose.pose.position.y) * cos(theta_t) + (odom_msg->pose.pose.position.x) * sin(theta_t) + y_t;
    }
    else {
        x = 0.0;
        y = 0.0;
    }
    if(team_color == "blue")
    {
        cur_position.x = y;
        cur_position.y = x;
        cur_position.z = 0.0;

    }
    else if (team_color == "yellow")
    {
        cur_position.x = 3*a - y;
        cur_position.y = b - x;
        cur_position.z = 0.0;
    }
    else {
        cur_position.x = 0.0;
        cur_position.y = 0.0;
        cur_position.z = 0.0;
    }
    position_pub.publish(cur_position);
    alive.data = true;
    alive_pub.publish(alive);
    //ROS_INFO_STREAM("x: " << x << " y: " << y);

}

void integration::statusCallback(const std_msgs::Bool& status_msg)
{
    status = status_msg.data;
}
bool integration::startCallback(leistung_integration::start::Request &req, leistung_integration::start::Response &res)
{
    start = req.start;
    return true;
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "integration");
    //ros::AsyncSpinner spinner(3);
    //spinner.start();
    ros::NodeHandle nh;
    integration node(nh);
    ros::spin();
    //ros::waitForShutdown();
  return 0;
}
