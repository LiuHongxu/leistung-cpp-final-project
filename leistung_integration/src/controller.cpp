#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <std_msgs/Bool.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CompressedImage.h>

#include <nav_msgs/Odometry.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
//include srv header
#include <leistung_integration/forward.h>
#include <leistung_integration/rotation.h>
#include <vector>
#include <math.h>

#include <leistung_integration/rotation_test.h>
#include <leistung_integration/loc_paramerts.h>

using namespace std;
using namespace cv;
using namespace cv_bridge;

class Controller
{
private:
  ros::NodeHandle nh_, priv_nh_;
  ros::Rate r();
  ros::Publisher vel_pub, point_pub, position_pub2, alive_pub2;
  ros::Subscriber depthImg_sub, CompDepthImg_sub, objectPos_sub, odom_sub;
  ros::ServiceServer rotation_srv, forward_srv, rotation_test, loc_paramerts_srv;
  ros::ServiceClient rot_cli;
  //cv_bridge::CvImagePtr cv_ptr;
  geometry_msgs::Twist vel, odom_vel, vel_msg;
  geometry_msgs::Point point;
  geometry_msgs::Pose odom_pose, origin_pose;
  geometry_msgs::Point cur_position2;
  std_msgs::Bool alive2;
  Mat depth_imgC, depth_img, depth_rect;
  float z, z_0;
  int x, y, x_, y_, time, time2, noDepth_times;
  double phi, goal_theta, goal_theta_, deta_theta, sin_phi2, x_cur, y_cur, desired_d , dis;
  string team_color;
  double a, b, x_t, y_t, theta_t;
  //------------------callback function---------------------//
  void depthImg_callback(const sensor_msgs::ImageConstPtr& depth);
  void CompDepthImg_callback(const sensor_msgs::CompressedImageConstPtr& depth);
  void objectPos_callback(const geometry_msgs::PointConstPtr& pos);
  void odom_callback(const nav_msgs::OdometryConstPtr& odom);
  bool rotation_callback(leistung_integration::rotation::Request &req, leistung_integration::rotation::Response &res);
  bool forward_callback(leistung_integration::forward::Request &req, leistung_integration::forward::Response &res);
  bool rotation_test_callback(leistung_integration::rotation_test::Request &req, leistung_integration::rotation_test::Response &res);
  bool loc_parameters_callback(leistung_integration::loc_paramerts::Request &req, leistung_integration::loc_paramerts::Response &res);
public:
  Controller(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
  {
    x = 0;
    y = 0;
    team_color = "unknown";
    depth_img = Mat::zeros(480, 640, CV_32FC1);
    depth_imgC = Mat::zeros(480, 640, CV_32FC1);
    depthImg_sub = nh_.subscribe("/camera/depth_registered/image_raw", 1, &Controller::depthImg_callback, this);
    CompDepthImg_sub = nh_.subscribe("/camera/depth/image_raw/compressed", 1, &Controller::CompDepthImg_callback, this);
    objectPos_sub = nh_.subscribe("/object_pose", 1, &Controller::objectPos_callback, this);
    odom_sub = nh_.subscribe("/odom", 1, &Controller::odom_callback, this);
    vel_pub = nh_.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);//turtle1/cmd_vel
    position_pub2 = nh_.advertise<geometry_msgs::Point>("/current_position_topic", 1);
    alive_pub2 = nh_.advertise<std_msgs::Bool>("/alive_status_topic", 1);
    rotation_srv = nh_.advertiseService("/rotation_turtlebot", &Controller::rotation_callback, this);
    forward_srv =nh_.advertiseService("/forward_turtlebot", &Controller::forward_callback, this);
    rotation_test = nh_.advertiseService("/rotation_turtlebot_test", &Controller::rotation_test_callback, this);
    loc_paramerts_srv = nh_.advertiseService("/loc_parameters", &Controller::loc_parameters_callback, this);
  }
  ~Controller(){}
};

bool Controller::loc_parameters_callback(leistung_integration::loc_paramerts::Request &req, leistung_integration::loc_paramerts::Response &res)
{
    a = req.a;
    b = req.b;
    x_t = req.x_t;
    y_t = req.y_t;
    theta_t = req.theta_t;
    team_color = req.teamcolor;
    //ROS_INFO_STREAM("x_t: " << x_t << " y_t: " << " a: " << a << " b: " << b << " theta_t: " << theta_t);
    return true;
}

bool Controller::rotation_test_callback(leistung_integration::rotation_test::Request &req, leistung_integration::rotation_test::Response &res)
{

    deta_theta = req.angle;
    goal_theta = phi + deta_theta;
    //ROS_INFO_STREAM("phi original" << phi);
    //ROS_INFO_STREAM("phi goal" << goal_theta);
    ros::Rate rate(20);

    double v_max;
    v_max = 1.3;


    if(goal_theta > 3.1416)
    {
        goal_theta_ = goal_theta - (2 * 3.1416);
    }
    else if(goal_theta < - 3.1416)
    {
        goal_theta_ = goal_theta + (2 * 3.1416);
    }
    else {
        goal_theta_ = goal_theta;
    }

    if(goal_theta > 3.1416 || goal_theta < - 3.1416)
    {
        while ( fabs(goal_theta_ - phi) >= 0.3)
        {
            if(goal_theta > 3.1416)
            {
                vel_msg.angular.z = v_max;
            }
            else if(goal_theta < - 3.1416)
            {
                vel_msg.angular.z = -v_max;

            }
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" phi : " << (goal_theta_ - phi) << ",  " << phi << "goal :" << goal_theta);
            rate.sleep();
            ros::spinOnce();

        }
        while (fabs(goal_theta_ - phi) > 0.04)
        {

            vel_msg.angular.z = (goal_theta_ - phi)/fabs(goal_theta_ - phi) * max(0.2, min(2 * sin(fabs(goal_theta_-phi)), v_max));
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" z: " << vel_msg.angular.z);
            //ROS_INFO_STREAM(" phi : " << (goal_theta_ - phi) << ",  " << phi);
            rate.sleep();
            ros::spinOnce();
        }
    }
    else {
        while (fabs(goal_theta_ - phi) > 0.04)
        {
            if(fabs(goal_theta_ - phi) > 0.3)
            {
                vel_msg.angular.z = (goal_theta_ - phi) / fabs(goal_theta_ - phi) * v_max;
            }
            else {
                vel_msg.angular.z = (goal_theta_ - phi)/fabs(goal_theta_ - phi) * max(0.2, min(2 * sin(fabs(goal_theta_-phi)), v_max));
            }
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" phi : " << phi);
            //ROS_INFO_STREAM(" z: " << vel_msg.angular.z);
            //ROS_INFO_STREAM(" phi : " << ((goal_theta_ / 3.1416 * 180) - req.angle) << ",  " << phi);
            rate.sleep();
            ros::spinOnce();
        }
    }
/*
    int k = 0;
    while (k < 1) {
        vel_msg.angular.z = 0;
        vel_pub.publish(vel_msg);
        k++;
        rate.sleep();
    }*/
    vel_msg.angular.z = 0;
    vel_pub.publish(vel_msg);
    res.done = true;
    return true;

}

void Controller::odom_callback(const nav_msgs::OdometryConstPtr& odom)
{
    sin_phi2 = odom->pose.pose.orientation.z;
    phi = 2 * asin(sin_phi2);//   -pi, +pi
    odom_pose.position.x = odom->pose.pose.position.x;
    odom_pose.position.y = odom->pose.pose.position.y;
    odom_pose.orientation.z = odom->pose.pose.orientation.z;
    odom_pose.orientation.w = odom->pose.pose.orientation.w;
    odom_vel.linear.x =  odom->twist.twist.linear.x;
    odom_vel.angular.z = odom->twist.twist.angular.z;
    // if(team_color != "unknown")
    // {
    //     x = (odom->pose.pose.position.x) * cos(theta_t) - (odom->pose.pose.position.y) * sin(theta_t) + x_t;
    //     y = (odom->pose.pose.position.y) * cos(theta_t) + (odom->pose.pose.position.x) * sin(theta_t) + y_t;
    //     if(team_color == "blue")
    //     {
    //         cur_position2.x = y;
    //         cur_position2.y = x;
    //         cur_position2.z = 0.0;
    //         //position_pub2.publish(cur_position2);
    //         //alive2.data = true;
    //         //alive_pub2.publish(alive2);

    //     }
    //     else
    //     {
    //         cur_position2.x = 3*a - y;
    //         cur_position2.y = b - x;
    //         cur_position2.z = 0.0;
    //         //position_pub2.publish(cur_position2);
    //         //alive2.data = true;
    //         //alive_pub2.publish(alive2);
    //     }
    // }
    // else {
    //     x = 0.0;
    //     y = 0.0;
    //     cur_position2.x = 0.0;
    //     cur_position2.y = 0.0;
    //     cur_position2.z = 0.0;
    // }



}

void Controller::depthImg_callback(const sensor_msgs::ImageConstPtr &depth)
{
    cv_bridge::CvImagePtr cv_ptr;
    try {
      cv_ptr = cv_bridge::toCvCopy(depth, sensor_msgs::image_encodings::TYPE_32FC1);

      depth_img = cv_ptr->image;
      //Rect rect(0, 150, 640, 250);
      //depth_rect = depth_img(rect);
      //namedWindow("original image", WINDOW_AUTOSIZE);
      //imshow("original image", Image);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("Could not convert from '%s' to 'bgr8'.", depth->encoding.c_str());

  }

}

void Controller::CompDepthImg_callback(const sensor_msgs::CompressedImageConstPtr& depth)
{
    try {
        depth_imgC = cv::imdecode(cv::Mat(depth->data ),1);
        //namedWindow("original image", WINDOW_AUTOSIZE);
        //imshow("original image", Image);
        //depth->header.
    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("Could not convert to image");

    }
}

void Controller::objectPos_callback(const geometry_msgs::PointConstPtr &pos)
{
    x = static_cast<int>(pos->x);
    y = static_cast<int>(pos->y);
    //ROS_INFO_STREAM( " empty : " << depth_img.empty());
    z = depth_img.at<float>(y+10, x+50);// y + 20
    //z = depth_imgC.at<float>(y+20, x);
    ROS_INFO_STREAM("x = " << x << ", y = "<< y << ", z = " << z);

 }
bool Controller::rotation_callback(leistung_integration::rotation::Request &req, leistung_integration::rotation::Response &res)
{
    ros::Rate rate(10);
    ros::Rate rate2(20);
    vel.linear.x = 0;
    time = 0;
    ros::spinOnce();
    while(req.start)
    {
        ros::spinOnce();
        ROS_INFO_STREAM("x = " << x << ", y =" << y);
        
        if (x == 0 && y == 0)
        {
            vel.angular.z= 0.2;
            res.distance_ = 0;
            res.find = false;
        }
        else if( x < 190)
        {
            vel.angular.z = 0.15;
            res.distance_ = 0;
            res.find = false;
        }
        else if ( x > 330) {
            vel.angular.z = -0.15;
            res.distance_ = 0;
            res.find = false;

        }
        else 
        {
            vel.angular.z= 0;
            ros::Duration(1.5).sleep();
            time ++;
            if( time >= 3)
            {
                noDepth_times = 0;
                while( z <= 0.45 || z!=z || z >= 3.5 && noDepth_times < 200)
                {
                    ROS_INFO_STREAM("depth not found ");
                    ROS_INFO_STREAM("z: " << z);
                    noDepth_times++;
		            rate2.sleep();
                    ros::spinOnce();
                }
                z_0 = z;
		        int t = 0;
                while( t <= 2)
                {
                    if(fabs(z-z_0)<0.05)
                    {
                        t++;
                    }
		            z_0 = z;                    
                    rate2.sleep();
                    ros::spinOnce();
                }

                if(noDepth_times == 200)
                {
		            if(z!=z || z <= 0.45)
                    {
                        res.distance_ = 0.6 ;
                    }
        		    else
		            {
                        res.distance_ = 0 ;
                    }
                }
                else 
                {
                    res.distance_ = z ;
                }
                res.find = true;
                break;
            }
        }
        vel_pub.publish(vel);
        rate.sleep();
        ros::spinOnce();
    }
    return res.find;
}
bool Controller::forward_callback(leistung_integration::forward::Request &req, leistung_integration::forward::Response &res)
{
    ros::Rate rate(10);
    vel.angular.z = 0;
    vel.linear.x = 0;
    double way = 0;
    origin_pose = odom_pose;
    way = sqrt( pow(origin_pose.position.x - odom_pose.position.x, 2) + pow(origin_pose.position.y - odom_pose.position.y, 2));
    ros::spinOnce();
    while( fabs(req.distance)-way >= 0.008)
    {

        if ( fabs(req.distance)-way > 0.2)
        {
            vel.linear.x = min(0.6, way + 0.3);
        }
        else
        {
            vel.linear.x = max(0.0001,  min(0.6, (fabs(req.distance)-way)*2)) ;

        }
        if(req.rotation)
        {
            if( x == 0 )
            {
                vel.angular.z = 0.0;
            }
            else if ( fabs(req.distance)-way < 0.4)
            {
                vel.angular.z = 0.0;
            }
            else {
                vel.angular.z =  - (1.1 * (x - 270)/270) * (fabs(req.distance)-way);
            }
        }
        else 
        {
            vel.angular.z = 0.0;
        }
        if(req.distance < 0)
        {
            vel.linear.x = - vel.linear.x;
        }

        vel_pub.publish(vel);
        rate.sleep();
        ros::spinOnce();
        way = sqrt( pow(origin_pose.position.x - odom_pose.position.x, 2) + pow(origin_pose.position.y - odom_pose.position.y, 2));

    }
    if ( fabs(req.distance)-way <= 0.01)
    {
        res.done = true;

    }
    else{
        res.done = false;
    }
    //ROS_INFO_STREAM("way = " << way);

    return true;
}

int main(int argc, char**argv)
{
  ros::init(argc, argv, "controller");
  //ros::AsyncSpinner spinner(3);
  //spinner.start();
  ros::NodeHandle nh;
  Controller node(nh);
  //ros::waitForShutdown();

  ros::spin();
  return 0;
}
